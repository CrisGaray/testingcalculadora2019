/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author natal
 */
public class Volumen extends javax.swing.JFrame {
    Calculadora c=new Calculadora();
    /**
     * Creates new form Volumen
     */
    public Volumen() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButtonVolver = new javax.swing.JButton();
        jButtonN5 = new javax.swing.JButton();
        jButtonN6 = new javax.swing.JButton();
        jButtonN7 = new javax.swing.JButton();
        jButtonN8 = new javax.swing.JButton();
        jButtonN0 = new javax.swing.JButton();
        jButtonBorrar1 = new javax.swing.JButton();
        jButtonN9 = new javax.swing.JButton();
        jButtonN1 = new javax.swing.JButton();
        jButtonPunto = new javax.swing.JButton();
        jButtonN2 = new javax.swing.JButton();
        jButtonBorrarTodo = new javax.swing.JButton();
        jButtonN3 = new javax.swing.JButton();
        jButtonN4 = new javax.swing.JButton();
        jTextPantalla1 = new javax.swing.JTextField();
        jBoxVolumen1 = new javax.swing.JComboBox<>();
        jBoxVolumen2 = new javax.swing.JComboBox<>();
        jLabelResultado = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("Calcular");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButtonVolver.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonVolver.setText("Volver");
        jButtonVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverActionPerformed(evt);
            }
        });

        jButtonN5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonN5.setText("5");
        jButtonN5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonN5ActionPerformed(evt);
            }
        });

        jButtonN6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonN6.setText("6");
        jButtonN6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonN6ActionPerformed(evt);
            }
        });

        jButtonN7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonN7.setText("7");
        jButtonN7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonN7ActionPerformed(evt);
            }
        });

        jButtonN8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonN8.setText("8");
        jButtonN8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonN8ActionPerformed(evt);
            }
        });

        jButtonN0.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonN0.setText("0");
        jButtonN0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonN0ActionPerformed(evt);
            }
        });

        jButtonBorrar1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonBorrar1.setText("<---");
        jButtonBorrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBorrar1ActionPerformed(evt);
            }
        });

        jButtonN9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonN9.setText("9");
        jButtonN9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonN9ActionPerformed(evt);
            }
        });

        jButtonN1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonN1.setText("1");
        jButtonN1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonN1ActionPerformed(evt);
            }
        });

        jButtonPunto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonPunto.setText(".");
        jButtonPunto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPuntoActionPerformed(evt);
            }
        });

        jButtonN2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonN2.setText("2");
        jButtonN2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonN2ActionPerformed(evt);
            }
        });

        jButtonBorrarTodo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonBorrarTodo.setText("C");
        jButtonBorrarTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBorrarTodoActionPerformed(evt);
            }
        });

        jButtonN3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonN3.setText("3");
        jButtonN3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonN3ActionPerformed(evt);
            }
        });

        jButtonN4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonN4.setText("4");
        jButtonN4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonN4ActionPerformed(evt);
            }
        });

        jTextPantalla1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextPantalla1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jBoxVolumen1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jBoxVolumen1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "cm³", "m³", "Litros", "Galones" }));

        jBoxVolumen2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jBoxVolumen2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "cm³", "m³", "Litros", "Galones" }));

        jLabelResultado.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelResultado.setForeground(new java.awt.Color(255, 0, 0));
        jLabelResultado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelResultado.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jBoxVolumen2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBoxVolumen1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonVolver))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(61, 61, 61))
            .addGroup(layout.createSequentialGroup()
                .addGap(142, 142, 142)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextPantalla1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jButtonN7)
                            .addGap(18, 18, 18)
                            .addComponent(jButtonN8, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jButtonN9))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jButtonN0)
                            .addGap(18, 18, 18)
                            .addComponent(jButtonPunto)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(3, 3, 3)
                            .addComponent(jButtonBorrar1))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jButtonN1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonN4))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jButtonN5)
                                    .addGap(18, 18, 18)
                                    .addComponent(jButtonN6))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jButtonN2)
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jButtonBorrarTodo)
                                        .addComponent(jButtonN3)))))))
                .addGap(51, 51, 51))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jTextPantalla1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jBoxVolumen1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(62, 62, 62)
                        .addComponent(jBoxVolumen2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41)
                        .addComponent(jLabelResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButtonVolver)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonBorrar1)
                            .addComponent(jButtonBorrarTodo))
                        .addGap(42, 42, 42)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonN1)
                            .addComponent(jButtonN2)
                            .addComponent(jButtonN3))
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonN4)
                            .addComponent(jButtonN5)
                            .addComponent(jButtonN6))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonN8)
                            .addComponent(jButtonN9)
                            .addComponent(jButtonN7))
                        .addGap(17, 17, 17)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonN0)
                            .addComponent(jButtonPunto))))
                .addGap(40, 40, 40))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonN5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonN5ActionPerformed
        jTextPantalla1.setText(jTextPantalla1.getText()+"5");
    }//GEN-LAST:event_jButtonN5ActionPerformed

    private void jButtonN6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonN6ActionPerformed
        jTextPantalla1.setText(jTextPantalla1.getText()+"6");
    }//GEN-LAST:event_jButtonN6ActionPerformed

    private void jButtonN7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonN7ActionPerformed
        jTextPantalla1.setText(jTextPantalla1.getText()+"7");
    }//GEN-LAST:event_jButtonN7ActionPerformed

    private void jButtonN8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonN8ActionPerformed
        jTextPantalla1.setText(jTextPantalla1.getText()+"8");
    }//GEN-LAST:event_jButtonN8ActionPerformed

    private void jButtonN0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonN0ActionPerformed
        jTextPantalla1.setText(jTextPantalla1.getText()+"0");
    }//GEN-LAST:event_jButtonN0ActionPerformed

    private void jButtonBorrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBorrar1ActionPerformed
        if(jTextPantalla1.getText().length() > 0){
            jTextPantalla1.setText(jTextPantalla1.getText().substring(0,jTextPantalla1.getText().length()-1));
        }
    }//GEN-LAST:event_jButtonBorrar1ActionPerformed

    private void jButtonN9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonN9ActionPerformed
        jTextPantalla1.setText(jTextPantalla1.getText()+"9");
    }//GEN-LAST:event_jButtonN9ActionPerformed

    private void jButtonN1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonN1ActionPerformed
        jTextPantalla1.setText(jTextPantalla1.getText()+"1");
    }//GEN-LAST:event_jButtonN1ActionPerformed

    private void jButtonPuntoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPuntoActionPerformed
        boolean p=false;
        for(int i=0; i<jTextPantalla1.getText().length(); i++){
            if(jTextPantalla1.getText().charAt(i) == '.'){
                p=true;
                break;
            }
        }

        if(!p){
            jTextPantalla1.setText(jTextPantalla1.getText()+'.');
        }
    }//GEN-LAST:event_jButtonPuntoActionPerformed

    private void jButtonN2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonN2ActionPerformed
        jTextPantalla1.setText(jTextPantalla1.getText()+"2");
    }//GEN-LAST:event_jButtonN2ActionPerformed

    private void jButtonBorrarTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBorrarTodoActionPerformed
        jTextPantalla1.setText("");
    }//GEN-LAST:event_jButtonBorrarTodoActionPerformed

    private void jButtonN3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonN3ActionPerformed
        jTextPantalla1.setText(jTextPantalla1.getText()+"3");
    }//GEN-LAST:event_jButtonN3ActionPerformed

    private void jButtonN4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonN4ActionPerformed
        jTextPantalla1.setText(jTextPantalla1.getText()+"4");
    }//GEN-LAST:event_jButtonN4ActionPerformed

    private void jButtonVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverActionPerformed
        Conversiones abrir=new Conversiones();
        abrir.setVisible(true);
        this.setVisible(false);
        abrir.setLocationRelativeTo(null);
    }//GEN-LAST:event_jButtonVolverActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(jBoxVolumen1.getSelectedItem()==jBoxVolumen2.getSelectedItem()){  
            c.setResultado(jTextPantalla1.getText());
            jLabelResultado.setText(c.getResultado());
        }
        
        //CM3
        if(jBoxVolumen1.getSelectedItem()=="cm³" && jBoxVolumen2.getSelectedItem()=="m³"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf(c.getN1()/1000000));
            jLabelResultado.setText(c.getResultado());
        }
        
        if(jBoxVolumen1.getSelectedItem()=="cm³" && jBoxVolumen2.getSelectedItem()=="Litros"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf(c.getN1()/1000));
            jLabelResultado.setText(c.getResultado());
        }
        
        if(jBoxVolumen1.getSelectedItem()=="cm³" && jBoxVolumen2.getSelectedItem()=="Galones"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf((c.getN1()*0.264)/1000));
            jLabelResultado.setText(c.getResultado());
        }
        
        //GALONES
        if(jBoxVolumen1.getSelectedItem()=="Galones" && jBoxVolumen2.getSelectedItem()=="cm³"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf((c.getN1()*1000)/0.26417));
            jLabelResultado.setText(c.getResultado());
        }
        
        if(jBoxVolumen1.getSelectedItem()=="Galones" && jBoxVolumen2.getSelectedItem()=="Litros"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf(c.getN1()*3.78541));
            jLabelResultado.setText(c.getResultado());
        }
        
        if(jBoxVolumen1.getSelectedItem()=="Galones" && jBoxVolumen2.getSelectedItem()=="m³"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf(c.getN1()*0.00378541));
            jLabelResultado.setText(c.getResultado());
        }
        
        //LITROS
        if(jBoxVolumen1.getSelectedItem()=="Litros" && jBoxVolumen2.getSelectedItem()=="cm³"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf(c.getN1()/0.001));
            jLabelResultado.setText(c.getResultado());
        }
        
        if(jBoxVolumen1.getSelectedItem()=="Litros" && jBoxVolumen2.getSelectedItem()=="m³"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf(c.getN1()/1000));
            jLabelResultado.setText(c.getResultado());
        }
        
        if(jBoxVolumen1.getSelectedItem()=="Litros" && jBoxVolumen2.getSelectedItem()=="Galones"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf(c.getN1()/3.7854));
            jLabelResultado.setText(c.getResultado());
        }
        //m3
        if(jBoxVolumen1.getSelectedItem()=="m³" && jBoxVolumen2.getSelectedItem()=="cm³"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf(c.getN1()*1000000));
            jLabelResultado.setText(c.getResultado());
        }
        
        if(jBoxVolumen1.getSelectedItem()=="m³" && jBoxVolumen2.getSelectedItem()=="Litros"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf(c.getN1()/0.001));
            jLabelResultado.setText(c.getResultado());
        }
        
        if(jBoxVolumen1.getSelectedItem()=="m³" && jBoxVolumen2.getSelectedItem()=="Galones"){
            c.setN1(Double.parseDouble(jTextPantalla1.getText()));
            c.setResultado(String.valueOf(c.getN1()/0.0037854));
            jLabelResultado.setText(c.getResultado());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Volumen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Volumen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Volumen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Volumen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Volumen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> jBoxVolumen1;
    private javax.swing.JComboBox<String> jBoxVolumen2;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonBorrar1;
    private javax.swing.JButton jButtonBorrarTodo;
    private javax.swing.JButton jButtonN0;
    private javax.swing.JButton jButtonN1;
    private javax.swing.JButton jButtonN2;
    private javax.swing.JButton jButtonN3;
    private javax.swing.JButton jButtonN4;
    private javax.swing.JButton jButtonN5;
    private javax.swing.JButton jButtonN6;
    private javax.swing.JButton jButtonN7;
    private javax.swing.JButton jButtonN8;
    private javax.swing.JButton jButtonN9;
    private javax.swing.JButton jButtonPunto;
    private javax.swing.JButton jButtonVolver;
    private javax.swing.JLabel jLabelResultado;
    private javax.swing.JTextField jTextPantalla1;
    // End of variables declaration//GEN-END:variables
}
